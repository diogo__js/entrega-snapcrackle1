function snapCrackle(maxValue){
    let string = ''
    for (let numero = 1; numero <= maxValue; numero++) {

        if(numero % 2 != 0 && numero % 5 == 0){
            string = string + "SnapCrackle, "   // É IMPAR E DIVISIVEL POR 5
        }

        else if(numero % 5 == 0){
            string = string + "Crackle, "   // É DIVISIVEL POR 5
        }
      
        else if(numero % 2 != 0){
            string = string + "Snap, "   // É IMPAR
        }

        else if(numero % 2 == 0 && numero % 5 != 0){
            string = string + numero + ", "   // NÃO É IMPAR, NEM DIVISIVEL POR 5
        }            
    }
    return console.log(string)
}
//*****************************************************************************************************/
function numeroPrimo(limite){
    for(let numero = 2; numero<limite; numero++){
        if(limite % numero === 0){
            return false;                      // FUNCAO QUE VERIFICA SE É PRIMO
        }
    }
    return true;
}
//*****************************************************************************************************/
function snapCracklePrime(maxValue){
    let string = ''

    for (let numero = 1; numero <= maxValue; numero++) {

        if(numero % 2 != 0 && numero % 5 == 0){               
            if(numeroPrimo(numero)){                          
                string = string + "SnapCracklePrime, "          // É IMPAR E DIVISIVEL POR 5
            }
            else{
                string = string + "SnapCrackle, "  
            }    
        }
//--------------------------------------------------------------------------------------------------
        else if(numero % 5 == 0){ 
            if(numeroPrimo(numero)){                  
                string = string + "CracklePrime, "
            }                                               // É DIVISIVEL POR 5
            else{
                string = string + "Crackle, "   
            }   
        }
//--------------------------------------------------------------------------------------------------
        else if(numero % 2 != 0){
            if(numeroPrimo(numero)){
                string = string + "SnapPrime, "
            }                                                   // É IMPAR
            else{
                string = string + "Snap, "   
            }       
        }
//--------------------------------------------------------------------------------------------------
        else if(numero % 2 == 0 && numero % 5 != 0){
            if(numeroPrimo(numero)){
                string = string + "Prime, " 
            }                                               // NÃO É IMPAR, NEM DIVISIVEL POR 5
            else{
                string = string + numero + ", "  
            }   
        }
//--------------------------------------------------------------------------------------------------

    }
    return console.log(string)
}
//*********************************************************************************************************************************

// CHAMANDO FUNÇÕES
//snapCrackle(12);   
 snapCracklePrime(15);


// TESTES ABAIXO PARA FUNÇÃO DE NUMERO PRIMO

// console.log(numeroPrimo(7))    //retorna TRUE porque 7 é primo
// console.log(numeroPrimo(9))   //retorna FALSE porque 9  não é primo

